import 'package:ceiba_flutter_test_applicant/app/models/post_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

/// Clase modelo del usuario autenticado
@JsonSerializable()
class User {

  /// Identificador
  @JsonKey(required: true)
  final int id;
  /// Nombre
  @JsonKey(required: true)
  final String name;
  /// Telefono
  @JsonKey(required: true)
  final String phone;
  /// Correo electronico
  @JsonKey(required: true)
  final String email;

  /// Constructor de la clase
  User({
    required this.id,
    required this.name,
    required this.phone,
    required this.email,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  String toString() {
    return 'User{id: $id, name: $name, phone: $phone, email: $email}';
  }
}
