import 'package:json_annotation/json_annotation.dart';

part 'post_model.g.dart';

/// Clase modelo publicaciones de usuario
@JsonSerializable()
class Post {

  /// Token de autenticacion de API
  @JsonKey(required: true)
  final String title;
  /// Codigo de acceso
  @JsonKey(required: true)
  final String body;

  /// Constructor de la clase
  Post({
    required this.title,
    required this.body
  });

  factory Post.fromJson(Map<String, dynamic> json) => _$PostFromJson(json);

  Map<String, dynamic> toJson() => _$PostToJson(this);

}
