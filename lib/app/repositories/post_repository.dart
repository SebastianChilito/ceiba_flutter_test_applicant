import 'dart:convert';

import 'package:ceiba_flutter_test_applicant/app/mixins/storage_mixin.dart';
import 'package:ceiba_flutter_test_applicant/app/models/post_model.dart';
import 'package:ceiba_flutter_test_applicant/app/services/post_service.dart';
import 'package:flutter_modular/flutter_modular.dart';

///
class PostRepository extends Disposable {

  /// Servicio de transacciones de publicaciones
  final PostService _postService;

  // Constructor de la clase
  PostRepository(this._postService);

  // Obtiene lista de publicaciones
  Future<List<Post>> posts() async {
    // Obtiene lista de publicaciones almacenadas
    return await _postService.posts();
  }

  // Obtiene lista de publicaciones de un usuario
  Future<List<Post>> postsByUser(int userId) async {
    // Obtiene lista de publicaciones almacenadas
    return await _postService.posts(userId: userId);
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }

}