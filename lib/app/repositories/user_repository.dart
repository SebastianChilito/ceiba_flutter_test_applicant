import 'dart:convert';
import 'dart:developer';

import 'package:ceiba_flutter_test_applicant/app/mixins/storage_mixin.dart';
import 'package:ceiba_flutter_test_applicant/app/models/user_model.dart';
import 'package:ceiba_flutter_test_applicant/app/services/user_service.dart';
import 'package:flutter_modular/flutter_modular.dart';

/// Repositorio para la obtencion de datos de usuarios
class UserRepository extends Disposable with StorageMixin {

  /// Servicio de transacciones de usuario
  final UserService _userService;

  // Constructor de la clase
  UserRepository(this._userService);

  // Obtiene lista de usuarios
  Future<List<User>> users() async {
    // Obtiene lista de usuarios almacenados localmente
    List<User> users = await _getStoredUsers();
    // Valida si no hay usuarios almacenados
    if (users.isEmpty) {
      // Obtiene usuarios del servidor
      users = await _userService.users();

      // Valida si la lista de usuarios No se encuentra vacia
      if (users.isNotEmpty) {
        // Almacena localmente los usuarios obtenidos
        await _saveUsers(users);
      }
    }

    return users;
  }

  /// Almacena lista de usuarios en el almacenamiento local
  /// [users] Lista de usuarios a alamacenar
  Future<bool> _saveUsers(List<User> users) async {
    // Guarda los datos de usuario
    return await store('users', jsonEncode(users));
  }

  /// Obtiene lista de usuarios almacenados en local
  Future<List<User>> _getStoredUsers() async {
    // Obtiene lista de usuarios
    String? strUsers = await get('users');

    // Retorna lista de usuarios alamcenados
    return (strUsers == null) ? [] : strUsers as List<User>;
  }


  @override
  void dispose() {
    // TODO
  }

}