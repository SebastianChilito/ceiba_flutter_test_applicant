import 'package:asuka/asuka.dart' as asuka;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Clase general de utilidades de la aplicacion
class Util {
  /// Permite crear un Material Color a partir de un color HEX
  static MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    final swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });
    return MaterialColor(color.value, swatch);
  }

  /// Visualiza un mensaje a el usuario
  static showMessage(String message,
      {Duration duration: const Duration(seconds: 3), bool isError: false}) {
    asuka.showSnackBar(SnackBar(
      backgroundColor: isError ? Colors.red : null,
      content: Text(message),
      duration: duration,
    ));
  }

  /// Visualiza un mensaje a el usuario
  static showMessageDialog({required String title, required String content, String btnText = 'Ok'}) {
    asuka.showDialog(
      builder: (context) => AlertDialog(
        title: Text(title),
        content: Text(content),
        actions: [
          TextButton(
              onPressed: () => Navigator.pop(context),
              child: Text(btnText)),
        ],
      ),
    );
  }

  /// Cargador opcional de asuka
  static var entry = OverlayEntry(
    builder: (context) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    },
  );

  /// Muestra cargador
  static showLoader() {
    asuka.addOverlay(entry);
  }

  /// Oculta cargador
  static hideLoader() {
    entry.remove();
  }

  /// Visualiza mensaje de confirmacion
  /// [tittle] titulo del mensaje
  /// [message] Descripcion del mensaje
  /// [onPositiveBtn] evento callback de boton positivo
  /// [onNegativeBtn] evento callback de boton negativo
  static showConfirmDialog(
      {required String tittle,
      required String message,
      required VoidCallback onPositiveBtn,
      VoidCallback? onNegativeBtn}) {
    asuka.showDialog(builder: (context) {
      return AlertDialog(
        title: Text(tittle),
        content: Text(message),
        actions: <Widget> [
          TextButton(child: Text('No'),  onPressed: () {
            Navigator.pop(context);
            if (onNegativeBtn != null) onNegativeBtn();
          }),
          TextButton(
            child: Text('Sí'),
            onPressed: () {
              Navigator.pop(context);
              onPositiveBtn();
            },
          )
        ],
      );
    });
  }

  /// Convierte valor entero o string verdadero a booleano
  static bool boolFromInt(dynamic state) {
    return state == 1 || state == 'true' || state == true;
  }

  /// Convierte valor boleano a entero
  static int boolToInt(bool state) => state ? 1 : 0;

}
