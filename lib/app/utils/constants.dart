import 'package:flutter/material.dart';

/// Colores generales de la aplicacion
class ThemeColors {
  static final primary = Color(0xff057B59);
}

/// Direccion base o dominio actual de conexion de datos con el servidor
const String basePath = 'https://jsonplaceholder.typicode.com/';
