import 'dart:developer';

import 'package:ceiba_flutter_test_applicant/app/models/post_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

/// Servicio de transacciones de publicaciones
class PostService {

  // Cliente http (Dio)
  Dio client = Modular.get<Dio>();

  /// Obtiene lista de publicaciones disponibles
  Future<List<Post>> posts({ int? userId }) async {
    // Ruta de acceso de publicaciones generales o publicaciones de un usuario
    final String endpoint = (userId == null) ? 'posts' : 'posts?userId=$userId';

    try {
      // Envia peticion para obtener lista de publicaciones
      Response response = await client.get(endpoint);
      // Organiza lista de publicaciones obtenidos
      return (response.data as List).map((user) {
        return Post.fromJson(user);
      }).toList();
    } catch (error, stacktrace) {
      log("Error DIO --posts--: $error stackTrace: $stacktrace");
    }
    return [];
  }


}