import 'dart:developer';

import 'package:ceiba_flutter_test_applicant/app/models/user_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

/// Servicio de transacciones de usuarios
class UserService {

  // Cliente http (Dio)
  Dio client = Modular.get<Dio>();

  /// Obtiene lista de usuarios disponibles
  Future<List<User>> users() async {

    try {
      // Envia peticion para obtener lista de publicaciones
      Response response = await client.get('users');
      // Organiza lista de usuarios obtenidos
      return (response.data as List).map((user) {
        return User.fromJson(user);
      }).toList();
    } catch (error, stacktrace) {
      log("Error DIO --users--: $error stackTrace: $stacktrace");
    }
    return [];
  }
}