import 'package:ceiba_flutter_test_applicant/app/repositories/user_repository.dart';
import 'package:ceiba_flutter_test_applicant/app/services/user_service.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'user_controller.dart';
import 'user_page.dart';

class UserModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => UserController(i.get())),
    Bind.lazySingleton((i) => UserRepository(i.get())),
    Bind.lazySingleton((i) => UserService()),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(Modular.initialRoute, child: (_, args) => const UserPage()),
  ];
}