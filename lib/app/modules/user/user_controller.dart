import 'dart:developer';

import 'package:ceiba_flutter_test_applicant/app/models/user_model.dart';
import 'package:ceiba_flutter_test_applicant/app/repositories/user_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:rxdart/rxdart.dart';

class UserController extends Disposable {

  /// Lista de usuarios disponibles
  final BehaviorSubject<List<User>> users$ = BehaviorSubject<List<User>>.seeded([]);
  /// Filtro de nombre de usuario
  final BehaviorSubject<String> nameUser$ = BehaviorSubject<String>.seeded('');

  /// Repositorio de usuarios
  final UserRepository _userRepository;

  /// Constructor de controldaor de usuarios
  UserController(this._userRepository);

  // Actualiza filtro de nombre de usuario
  set setNameUser(String name) {
    nameUser$.add(name);
  }

  /// Obtene usuarios filtrados por nombre de usuario
  get filterUsers => users$.value.where((User user) {
      return(nameUser$.value == '') ?
        true : user.name.toLowerCase().contains(nameUser$.value.toLowerCase());
  }).toList();

  /// Obtiene lista de usuarios disponibles
  Future<void> users() async {
    // Obtiene lista de usuarios disponibles y los agrega al stream de lista de usuarios
    users$.add(await _userRepository.users());
  }

  /// Obtiene lista de usuarios filtrados por nombre
  void filterByName(String? name)  {
    /// Valida si el filtro de busqueda por nombre de usuario
    /// NO Se encuentra vacio
    if (name != '' || name != null) {
      nameUser$.add(name!);
    }
  }

  @override
  void dispose() {
    users$.close();
  }
}