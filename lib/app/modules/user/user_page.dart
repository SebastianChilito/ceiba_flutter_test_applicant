import 'dart:developer';

import 'package:ceiba_flutter_test_applicant/app/models/user_model.dart';
import 'package:ceiba_flutter_test_applicant/app/modules/user/user_controller.dart';
import 'package:ceiba_flutter_test_applicant/app/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class UserPage extends StatefulWidget {
  final String title;
  const UserPage({Key? key, this.title = "Usuarios"}) : super(key: key);

  @override
  _UserPageState createState() => _UserPageState();
}

/// Pagina principal para visualizar lista de usuarios
class _UserPageState extends ModularState<UserPage, UserController> {

  // Controlador de campo de texto para filtro de usuario por nombre
  final TextEditingController _nameUserFieldCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: TextFormField(
              controller: _nameUserFieldCtrl,
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.only(left: 8, right: 8),
                labelText: 'Buscar usuario',
                helperText: 'Ingrese el nombre del usuario que desea buscar',
              ),
              keyboardType: TextInputType.text,
              onChanged: (name) {
                // Efectua filtro de busqueda por nombre
                store.filterByName(name);
              },
            ),
          ),
          Flexible(
            child: FutureBuilder(
              // Obtiene lista de usuarios disponibles
              future: store.users(),
              builder: (context, snapshot) {
                // Valida si se completa la obtencion de datos
                if (snapshot.connectionState == ConnectionState.done) {
                  return StreamBuilder(
                    stream: store.nameUser$.stream,
                    builder: (context, AsyncSnapshot<String> snapshot) {
                      // Obtiene lista de usuaros filtrados por nombre
                      List<User> users = store.filterUsers;
                      // Construye lista de usuarios
                      return buildUserList(users);
                    },
                  );
                }

                return const Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
          )
        ],
      )
    );
  }


  /// Construye lista de usuarios obtenidos
  /// [users] Lista de usuarios a visualizar
  Widget buildUserList(List<User> users) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: users.length,
      itemBuilder: (context, index) {
        User user = users[index];
        return Padding(
          padding: const EdgeInsets.all(8),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget> [
                  Text(user.name, style: TextStyle(
                    fontSize: 18,
                    color: ThemeColors.primary
                  )),
                  Row(children: <Widget> [
                    Padding(
                      padding: const EdgeInsets.only(right: 8, top: 4),
                      child: Icon(Icons.email, color: ThemeColors.primary),
                    ),
                    Text(user.phone),
                  ]),
                  Row(children: <Widget> [
                    Padding(
                      padding: const EdgeInsets.only(right: 8, top: 4),
                      child: Icon(Icons.phone, color: ThemeColors.primary),
                    ),
                    Text(user.email),
                  ]),

                  TextButton(
                      onPressed: () {
                        Modular.to.pushNamed('/posts', arguments: user);
                      },
                      child: const Align(
                        alignment: Alignment.centerRight,
                        child: Text('VER PUBLICACIONES'),
                      ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

}