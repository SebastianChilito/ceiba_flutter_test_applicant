import 'package:ceiba_flutter_test_applicant/app/repositories/post_repository.dart';
import 'package:ceiba_flutter_test_applicant/app/services/post_service.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'post_controller.dart';
import 'post_page.dart';

class PostModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => PostController(i.get())),
    Bind.lazySingleton((i) => PostRepository(i.get())),
    Bind.lazySingleton((i) => PostService()),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(Modular.initialRoute, child: (_, args) => PostPage(args.data)),
  ];
}