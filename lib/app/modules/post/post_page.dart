
import 'package:ceiba_flutter_test_applicant/app/models/post_model.dart';
import 'package:ceiba_flutter_test_applicant/app/models/user_model.dart';
import 'package:ceiba_flutter_test_applicant/app/modules/post/post_controller.dart';
import 'package:ceiba_flutter_test_applicant/app/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class PostPage extends StatefulWidget {
  final String title;
  final User user;

  const PostPage(this.user, {Key? key, this.title = "Publicaciones"}) : super(key: key);

  @override
  _PostPageState createState() => _PostPageState();
}

/// Pagina principal para ver las publicaciones de un usuario
class _PostPageState extends ModularState<PostPage, PostController> {

  @override
  void initState() {
    // Consulta publicaciones de un usuario
    store.postsByUser(widget.user.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget> [
                    Text(widget.user.name, style: TextStyle(
                        fontSize: 18,
                        color: ThemeColors.primary
                    )),
                    Row(children: <Widget> [
                      Padding(
                        padding: const EdgeInsets.only(right: 8, top: 4),
                        child: Icon(Icons.email, color: ThemeColors.primary),
                      ),
                      Text(widget.user.phone),
                    ]),
                    Row(children: <Widget> [
                      Padding(
                        padding: const EdgeInsets.only(right: 8, top: 4),
                        child: Icon(Icons.phone, color: ThemeColors.primary),
                      ),
                      Text(widget.user.email),
                    ]),
                  ],
                ),
              ),
            ),
          ),
          Flexible(
            child: StreamBuilder<List<Post>>(
              // Obtiene lista de publicaciones de usuario disponibles
              stream: store.posts$.stream,
              builder: (context, AsyncSnapshot<List<Post>> snapshot) {
                // Valida si se completa la obtencion de datos
                if (snapshot.hasData) {
                  List<Post> users = snapshot.data!;
                  // Construye lista de publicaciones
                  return buildPostList(users);
                }

                return const Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
          )
        ],
      ),
    );
  }


  /// Construye lista de publicaciones obtenidos
  /// [posts] Lista de publicaciones a visualizar
  Widget buildPostList(List<Post> posts) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: posts.length,
      itemBuilder: (context, index) {
        Post post = posts[index];
        return Padding(
          padding: const EdgeInsets.all(8),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget> [
                  Text(post.title, style: TextStyle(
                      fontSize: 18,
                      color: ThemeColors.primary
                  )),
                  Text(post.body, textAlign: TextAlign.justify,),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

}