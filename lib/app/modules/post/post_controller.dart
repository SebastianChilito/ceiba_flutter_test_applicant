import 'package:ceiba_flutter_test_applicant/app/models/post_model.dart';
import 'package:ceiba_flutter_test_applicant/app/repositories/post_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:rxdart/rxdart.dart';

/// Controlador de la vista de publicaciones de un usuario
class PostController extends Disposable {

  /// Lista de publicaciones disponibles
  final BehaviorSubject<List<Post>> posts$ = BehaviorSubject<List<Post>>();

  /// Repositorio de publicaciones
  final PostRepository _postRepository;

  /// Constructor de controlador de publicaciones
  PostController(this._postRepository);

  /// Obtiene lista de publicaciones basadas en el identificador del usuario
  Future<void> postsByUser(int userId) async {
    // Agrega lista de publicaciones encontradas al stream de publicaciones
    posts$.add(await _postRepository.postsByUser(userId));
  }

  @override
  void dispose() {
    posts$.close();
  }
}