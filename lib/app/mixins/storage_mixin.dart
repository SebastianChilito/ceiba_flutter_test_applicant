import 'dart:convert';
import 'dart:developer';

import 'package:shared_preferences/shared_preferences.dart';

/// Administrador de almacenamiento local de datos
mixin StorageMixin {

  // Almacena datos en alamacenamiento local
  Future<bool> store(String key, String data) async {
    // Obtiene instancia de almacenamiento interno
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // Elimina todos los datos almacenados
    await prefs.remove(key);
    // Guarda los datos
    return await prefs.setString('token', data);
  }

  // Obtiene datos de almacenamiento local
  Future<String?> get(String key) async {
    // Obtiene instancia de almacenamiento interno
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // Valida si existen datos alamacenados localmente
    if (prefs.containsKey(key) && prefs.getString(key) != 'null') {
      //log('prefs.getString(key)! ${jsonDecode(prefs.getString(key)!)}');
      // Obtiene datos almacenados
      return jsonDecode(prefs.getString(key)!);
    }
    return null;
  }

}