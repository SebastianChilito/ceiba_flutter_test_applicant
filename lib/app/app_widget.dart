import 'package:ceiba_flutter_test_applicant/app/utils/constants.dart';
import 'package:ceiba_flutter_test_applicant/app/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:asuka/asuka.dart' as asuka;

class AppWidget extends StatelessWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ceiba Flutter Test Applicant',
      builder: asuka.builder,
      theme: ThemeData(
        primarySwatch: Util.createMaterialColor(ThemeColors.primary),
      ),
      navigatorObservers: [
        asuka.asukaHeroController // Controlador general de paquete asuka
      ],
    ).modular();
  }
}