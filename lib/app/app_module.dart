import 'dart:io';

import 'package:ceiba_flutter_test_applicant/app/modules/post/post_module.dart';
import 'package:ceiba_flutter_test_applicant/app/modules/user/user_module.dart';
import 'package:ceiba_flutter_test_applicant/app/utils/constants.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppModule extends Module {

  Dio getDioConfig () {

    Dio dio = Dio(BaseOptions(
        baseUrl: basePath, // Local
        responseType: ResponseType.json));

    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };

    return dio;
  }

  @override
  final List<Bind> binds = [
    Bind((i) => AppModule().getDioConfig()),
  ];

  @override
  final List<ModularRoute> routes = [
    ModuleRoute(Modular.initialRoute, module: UserModule()),
    ModuleRoute('/posts', module: PostModule()),
  ];

}